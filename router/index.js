const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");


//comicion luz
router.get("/pagoLuz",(req,res)=>{
    const valores = {
        numReci:req.query.numReci,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        tipoServ:req.query.tipoServ,
        kiloW:req.query.kiloW,
        subtotal:req.query.subtotal,
        impuesto:req.query.impuesto,
        totalPagar:req.query.totalPagar,
        precio:req.query.precio,
        descuento:req.query.descuento
    }
    res.render('pagoLuz.html',valores);
});

router.post("/pagoLuz",(req,res)=>{
    const valores = {
        numReci:req.body.numReci,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        tipoServ:req.body.tipoServ,
        kiloW:req.body.kiloW,
        subtotal:req.body.subtotal,
        impuesto:req.body.impuesto,
        totalPagar:req.body.totalPagar,
        precio:req.body.precio,
        descuento:req.body.descuento
    }
    res.render('salidaDatos.html',valores);
});

 
router.get("/salidaDatos",(req,res)=>{
    const valores = {
        numReci:req.query.numReci,
       nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        tipoServ:req.query.tipoServ,
        kiloW:req.query.kiloW,
       subtotal:req.query.subtotal,
       impuesto:req.query.impuesto,
       totalPagar:req.query.totalPagar,
       precio:req.query.precio,
       descuento:req.query.descuento
   }
    res.render('salidaDatos.html',valores);
});

router.post("/salidaDatos",(req,res)=>{
    const valores = {
        numReci:req.body.numReci,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        tipoServ:req.body.tipoServ,
        kiloW:req.body.kiloW,
        subtotal:req.body.subtotal,
        impuesto:req.body.impuesto,
        totalPagar:req.body.totalPagar,
        precio:req.body.precio,
        descuento:req.body.descuento
        
    }
    res.render('salidaDatos.html',valores);
});

module.exports = router;